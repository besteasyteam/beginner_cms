﻿using Beginner.Infrastructure;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.CMS.Domain.Models.Authorizes
{
    /// <summary>
    /// 组织架构实体
    /// </summary>
    [TableName("tb_Auth_Organizes")]
    [PrimaryKey(primaryKey: "Id", AutoIncrement = true)]
    public class Organize : EntityBase<long>
    {
        /// <summary>
        /// 组织名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 所属上级ID
        /// </summary>
        public long ParentId { get; set; }
        /// <summary>
        /// 所属上级名称
        /// </summary>
        public string ParentName { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateUser { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public string UpdateUser { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateTime { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}

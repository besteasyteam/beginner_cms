﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;
using Beginner.Infrastructure;

namespace Beginner.CMS.Domain.Models.Authorizes
{
    /// <summary>
    /// 多对多映射实体
    /// </summary>
    [TableName("tb_Auth_ManyToManys")]
    [PrimaryKey(primaryKey: "Id", AutoIncrement = true)]
    public class ManyToMany : EntityBase<long>
    {
        /// <summary>
        /// 主键一
        /// </summary>
        public long PrimaryKeyOne { get; set; }
        /// <summary>
        /// 主键二
        /// </summary>
        public long PrimaryKeyTwo { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public ManyToManyType Type { get; set; }
        /// <summary>
        /// 关联时间
        /// </summary>
        public DateTime JoinTime { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }

}

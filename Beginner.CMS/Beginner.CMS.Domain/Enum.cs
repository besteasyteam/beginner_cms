﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.CMS.Domain
{
    /// <summary>
    /// 多对多映射类型
    /// </summary>
    public enum ManyToManyType
    {
        /// <summary>
        /// 一个用户可以拥有多个角色
        /// </summary>
        UserInRole = 1,
        /// <summary>
        /// 一个用户可以拥有多个组织
        /// </summary>
        UserInOrganize = 2
    }

    /// <summary>
    /// 状态
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// 启用
        /// </summary>
        Enable = 1,
        /// <summary>
        /// 禁用
        /// </summary>
        Disable = 2
    }
}

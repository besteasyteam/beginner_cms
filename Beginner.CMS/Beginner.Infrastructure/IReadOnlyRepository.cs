﻿using NPoco.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.Infrastructure
{
    /// <summary>
    /// 只读仓储接口
    /// </summary>
    /// <typeparam name="T">实体类型</typeparam>
    /// <typeparam name="TId"></typeparam>
    public interface IReadOnlyRepository<T, in TId> where T : IAggregateRoot
    {
        /// <summary>
        /// 获取一个对象
        /// </summary>
        /// <param name="id">主键ID</param>
        /// <returns></returns>
        T GetById(TId id);
        /// <summary>
        /// [异步]获取一个对象
        /// </summary>
        /// <param name="id">主键ID</param>
        /// <returns></returns>
        Task<T> GetByIdAsync(long id);

        /// <summary>
        /// 获取所有数据
        /// </summary>
        /// <returns>结果集</returns>
        IEnumerable<T> GetAll();
        /// <summary>
        /// [异步]获取所有数据
        /// </summary>
        /// <returns>结果集</returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// [Linq]查询数据
        /// </summary>
        /// <returns></returns>
        IQueryProviderWithIncludes<T> Query();
    }
}

﻿using NPoco.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.Infrastructure
{
    /// <summary>
    /// 仓储接口
    /// </summary>
    /// <typeparam name="T">实体对象类型</typeparam>
    /// <typeparam name="TId"></typeparam>
    public interface IRepository<T, in TId> : IReadOnlyRepository<T, TId> where T : IAggregateRoot
    {
        /// <summary>
        /// 添加一条记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        bool Insert(T entity);
        /// <summary>
        /// 添加一条记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="type">主键类型</param>
        /// <returns></returns>
        bool Insert(T entity, KeyType type);
        /// <summary>
        /// [异步]添加一条记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        Task<bool> InsertAsync(T entity);
        /// <summary>
        /// [异步]添加一条记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="type">主键类型</param>
        /// <returns></returns>
        Task<bool> InsertAsync(T entity, KeyType type);
        /// <summary>
        /// 批量添加记录
        /// </summary>
        /// <param name="entitys"></param>
        void InsertBulk(IEnumerable<T> entitys);
        /// <summary>
        /// 批量添加记录
        /// </summary>
        /// <param name="entitys"></param>
        /// <param name="type">主键类型</param>
        void InsertBulk(IEnumerable<T> entitys, KeyType type);
        /// <summary>
        /// 更新一条记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        bool Update(T entity);
        /// <summary>
        /// [异步]更新一条记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        Task<bool> UpdateAsync(T entity);
        /// <summary>
        /// 删除一条记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        bool Delete(T entity);
        /// <summary>
        /// [异步]删除一条记录
        /// </summary>
        /// <param name="entity">实体对象</param>
        Task<bool> DeleteAsync(T entity);
        /// <summary>
        /// 删除一条记录
        /// </summary>
        /// <param name="primaryKey">主键ID</param>
        /// <returns></returns>
        bool Delete(TId primaryKey);
        /// <summary>
        /// [Linq]修改数据
        /// </summary>
        /// <returns></returns>
        IUpdateQueryProvider<T> UpdateMany();
        /// <summary>
        /// [Linq]删除数据
        /// </summary>
        /// <returns></returns>
        IDeleteQueryProvider<T> DeleteMany();
        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Save(T entity);
    }
    /// <summary>
    /// 主键类型
    /// </summary>
    public enum KeyType
    {
        /// <summary>
        /// 自增
        /// </summary>
        Identity = 1,
        /// <summary>
        /// GUID
        /// </summary>
        Guid = 2
    }
}

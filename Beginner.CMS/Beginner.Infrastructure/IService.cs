﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Beginner.Infrastructure.Messages;

namespace Beginner.Infrastructure
{
    /// <summary>
    /// 服务接口
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public interface IService<TView, TId> where TView : ViewBase<TId>
    {
        /// <summary>
        /// 物理删除数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResponseBase Delete(TId id);
        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        ResponseBase Update(TView view);
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        ResponseBase Insert(TView view);
        /// <summary>
        /// 获取单个对象
        /// </summary>
        /// <param name="id">主键ID</param>
        /// <returns></returns>
        FindByResponse<TView,TId> GetById(TId id);
        /// <summary>
        /// 获取所有数据
        /// </summary>
        /// <returns></returns>
        FindAllResponse<TView, TId> FindAll();
    }
}

﻿using Beginner.Infrastructure;
using System.Collections.Generic;

namespace Beginner.Infrastructure.Messages
{
    /// <summary>
    /// 获取所有数据
    /// </summary>
    /// <typeparam name="TView">实体对象类型</typeparam>
    /// <typeparam name="TId">主键类型</typeparam>
    public class FindAllResponse<TView, TId> : ResponseBase where TView : ViewBase<TId>
    {
        /// <summary>
        /// 结果集
        /// </summary>
        public IEnumerable<TView> Items { get; set; }
    }
}

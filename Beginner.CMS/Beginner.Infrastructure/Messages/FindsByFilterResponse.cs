﻿using System.Collections.Generic;
using Beginner.Infrastructure;

namespace Beginner.Infrastructure.Messages
{
    /// <summary>
    /// 根据条件获取数据
    /// </summary>
    /// <typeparam name="TView">实体对象类型</typeparam>
    /// <typeparam name="TId">主键类型</typeparam>
    public class FindsByFilterResponse<TView, TId> : ResponseBase where TView : ViewBase<TId>
    {
        /// <summary>
        /// 结果集
        /// </summary>
        public IEnumerable<TView> Items { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Beginner.Infrastructure.Messages
{
    /// <summary>
    /// [分页]请求基类
    /// </summary>
    public class GetPageRequestBase
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="pageSize">每页数据数</param>
        public GetPageRequestBase(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }
        /// <summary>
        /// 当前页码
        /// </summary>
        public int PageIndex { get; private set; }
        /// <summary>
        /// 每页记录数
        /// </summary>
        public int PageSize { get; private set; }
    }
}

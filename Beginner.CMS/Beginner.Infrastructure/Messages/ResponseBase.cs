﻿namespace Beginner.Infrastructure.Messages
{
    /// <summary>
    /// 响应基类
    /// </summary>
    public class ResponseBase
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// 信息
        /// </summary>
        public string Message { get; set; }
    }
}

﻿using Beginner.Infrastructure;

namespace Beginner.Infrastructure.Messages
{
    /// <summary>
    /// 获取单个对象
    /// </summary>
    /// <typeparam name="TView">实体对象类型</typeparam>
    /// <typeparam name="TId">主键类型</typeparam>
    public class FindByResponse<TView, TId> : ResponseBase where TView : ViewBase<TId>
    {
        /// <summary>
        /// 实体对象
        /// </summary>
        public TView Item { get; set; }
    }
}

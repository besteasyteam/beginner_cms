﻿using Beginner.Infrastructure;
using NPoco;

namespace Beginner.Infrastructure.Messages
{
    /// <summary>
    /// 根据条件获取分页数据
    /// </summary>
    /// <typeparam name="TView">实体对象类型</typeparam>
    /// <typeparam name="TId">主键类型</typeparam>
    public class FindPageByFilterResponse<TView, TId> : ResponseBase where TView : ViewBase<TId>
    {
        /// <summary>
        /// 分页对象
        /// </summary>
        public Page<TView> Page { get; set; }
    }
}

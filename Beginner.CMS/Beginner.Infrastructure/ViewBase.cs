﻿namespace Beginner.Infrastructure
{
    public class ViewBase<TId>
    {
        public TId Id { get; set; }
    }
}

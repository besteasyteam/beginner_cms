﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Beginner.Infrastructure.Helper
{
    /// <summary>
    /// 数据格式化帮助类
    /// </summary>
    public class DataHelper
    {
        private DataHelper(){}
        /// <summary>
        /// 构建类的实例对象
        /// </summary>
        public static DataHelper Instance
        {
            get { return new DataHelper(); }
        }

        /// <summary>
        /// 获取结果集
        /// </summary>
        /// <param name="rel">状态</param>
        /// <param name="msg">提示信息</param>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public Dictionary<string, object> GetResult(bool rel, string msg, object data)
        {
            return new Dictionary<string, object> { { "rel", rel }, { "msg", msg }, { "obj", data } };
        }
        /// <summary>
        /// 获取结果集
        /// </summary>
        /// <param name="rel">状态</param>
        /// <param name="msg">提示信息</param>
        /// <returns></returns>
        public Dictionary<string, object> GetResult(bool rel, string msg)
        {
            return GetResult(rel, msg, null);
        }
        /// <summary>
        /// 获取结果集
        /// </summary>
        /// <param name="rel">状态</param>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public Dictionary<string, object> GetResult(bool rel, object data)
        {
            return new Dictionary<string, object> { { "rel", rel }, { "obj", data } };
        }
        /// <summary>
        /// 获取JsonString
        /// </summary>
        /// <param name="rel">状态</param>
        /// <param name="msg">提示信息</param>
        /// <param name="data">数据集</param>
        /// <returns></returns>
        public string GetJson(bool rel, string msg, object data)
        {
            return JsonConvert.SerializeObject(new Dictionary<string, object> { { "rel", rel }, { "msg", msg }, { "obj", data } });
        }
    }
}

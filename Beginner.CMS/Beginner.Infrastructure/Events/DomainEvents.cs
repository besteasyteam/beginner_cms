﻿namespace Beginner.Infrastructure.Events
{
    /// <summary>
    /// 
    /// </summary>
    public static class DomainEvents
    {
        /// <summary>
        /// 
        /// </summary>
        public static IDomainEventHandlerFactory DomainEventHandlerFactory { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="domainEvent"></param>
        public static void Raise<T>(T domainEvent) where T : IDomainEvent
        {
            DomainEventHandlerFactory.GetDomainEventHandlersFor(domainEvent).ForEach(h => h.Handle(domainEvent));
        }
    }

}

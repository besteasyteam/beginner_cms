﻿using System.Collections.Generic;

namespace Beginner.Infrastructure.Events
{
    /// <summary>
    /// 领域事件处理工厂接口
    /// </summary>
    public interface IDomainEventHandlerFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="domainEvent"></param>
        /// <returns></returns>
        IEnumerable<IDomainEventHandler<T>> GetDomainEventHandlersFor<T>(T domainEvent) where T : IDomainEvent;
    }

}

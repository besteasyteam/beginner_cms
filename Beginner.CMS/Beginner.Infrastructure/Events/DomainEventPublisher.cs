﻿namespace Beginner.Infrastructure.Events
{
    /// <summary>
    /// 
    /// </summary>
    public class DomainEventPublisher : IDomainEventPublisher
    {

        private readonly IDomainEventHandlerFactory _domainEventHandlerFactory;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="domainEventHandlerFactory"></param>
        public DomainEventPublisher(IDomainEventHandlerFactory domainEventHandlerFactory) {
            _domainEventHandlerFactory = domainEventHandlerFactory;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="domainEvent"></param>
        public void Publish<T>(T domainEvent) where T : IDomainEvent
        {
            _domainEventHandlerFactory.GetDomainEventHandlersFor(domainEvent).ForEach(h => h.Handle(domainEvent));
        }
    }

}

﻿namespace Beginner.Infrastructure.Events
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDomainEventHandler<in T> where T : IDomainEvent
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="domainEvent"></param>
        void Handle(T domainEvent);
    }

}

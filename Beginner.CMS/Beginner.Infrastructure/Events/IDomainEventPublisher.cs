﻿namespace Beginner.Infrastructure.Events
{
    /// <summary>
    /// 领域事件发布者接口
    /// </summary>
    public interface IDomainEventPublisher
    {
        /// <summary>
        /// 发布事件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="domainEvent"></param>
        void Publish<T>(T domainEvent) where T : IDomainEvent;
    }
}

﻿using Beginner.Infrastructure.Engines;
using System.Collections.Generic;

namespace Beginner.Infrastructure.Events
{
    /// <summary>
    /// 
    /// </summary>
    public class DomainEventHandlerFactory: IDomainEventHandlerFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="domainEvent"></param>
        /// <returns></returns>
        public IEnumerable<IDomainEventHandler<T>> GetDomainEventHandlersFor<T>(T domainEvent) where T : IDomainEvent
        {
            return EngineContainerFactory.GetContainer().GetAllInstances<IDomainEventHandler<T>>();
        }
    }
}
